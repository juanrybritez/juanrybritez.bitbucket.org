var Contacto = Backbone.Model.extend({
	defaults: { id:0, nombre: '', apellido: '', telefono:0, email:'', fechaCreacion: new Date()},
	urlRoot: "../../data/contactos",
	parse : function(response)
	{
		if($.isArray(response))
			return response[0];

		return response;
	}
});

var ContactoList = Backbone.Collection.extend({
	model: Contacto,
	url: "static/data/contactos.json",

	sortOrder: "asc",
	sortField: "apellido",
	/*Metodos a implementar Comparator y Search*/
	comparator: function (contacto1, contacto2)
	{
		if(this.sortOrder === "asc")
		{
			return contacto1.get(this.sortField) > contacto2.get(this.sortField);

		}else //this.sortOder === "desc"
		{
			return contacto1.get(this.sortField) < contacto2.get(this.sortField);
		}
	},

	search: function(letters)
	{
		if(letters === "") return this;

		//filtramos por regexp, i flag para ignore case (no distinguir lowercase/uppercase)
		var pattern = new RegExp(letters,'i');

		//iteramos la coll
		var filteredList = this.filter(function(data)
		{
			return (pattern.test( data.get('nombre') + "  "+data.get('apellido') ));
			//podríamos filtrar por editorial también
			//return ( pattern.test(data.get('autor')) || pattern.test(data.get('titulo')) );// || pattern.test(data.get('editorial')) );
		});

		//create new coll con los elementos filtrados
		var coll = new ContactoList(filteredList);

		return coll;

	}
});
